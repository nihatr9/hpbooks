//
//  UIButton+Cart.swift
//  HPBooks
//
//  Created by Nihed Majdoub on 02/04/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import UIKit

extension UIButton {
    
    static func makeAddToCart() -> UIButton {
        let view = UIButton()
        view.backgroundColor = UIColor.clear
        view.layer.backgroundColor = UIColor.white.cgColor
        let title = NSAttributedString(string: "add_cart_text".localized.uppercased(),
                                       attributes: [ NSAttributedStringKey.font : UIFont.systemFont(ofSize: 12, weight: .black),
                                                     NSAttributedStringKey.foregroundColor : UIColor.defaultTintColor()])
        view.setAttributedTitle(title, for: .normal)
        view.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        return view
    }
}
