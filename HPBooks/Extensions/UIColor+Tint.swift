//
//  UIColor+Tint.swift
//  HPBooks
//
//  Created by Nihed Majdoub on 02/04/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import UIKit

extension UIColor {
    
    static func defaultTintColor() -> UIColor {
        return UIColor(red: 0, green: 122/255, blue: 1.0, alpha: 1.0)
    }
}
