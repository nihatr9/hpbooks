//
//  UIViewController+Alert.swift
//  HPBooks
//
//  Created by Nihed Majdoub on 30/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showSipmleAlertVC(title: String, message: String, handler: (() -> ())?) {
        
        let alertVc = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let doneAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            handler?()
        }
        
        alertVc.addAction(doneAction)
        
        present(alertVc, animated: true, completion: nil)
    }
    
    func showActionVC(title: String,
                      message: String,
                      actionText: String,
                      source: UIBarButtonItem? = nil,
                      handler: (() -> ())?) {
        
        let alertVc = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        alertVc.popoverPresentationController?.barButtonItem = source
        
        let doneAction = UIAlertAction(title: actionText, style: .destructive) { (action) in
            handler?()
        }
        
        let cancelAction = UIAlertAction(title: "cancel_text".localized, style: .cancel) { (action) in
        }
        
        alertVc.addAction(cancelAction)
        alertVc.addAction(doneAction)
        
        present(alertVc, animated: true, completion: nil)
    }
}
