//
//  String+Localization.swift
//  HPBooks
//
//  Created by Nihed Majdoub on 30/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import Foundation

extension String {
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
