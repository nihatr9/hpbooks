//
//  UIView+Round.swift
//  HPBooks
//
//  Created by Nihed Majdoub on 28/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import UIKit

extension UIView {
    
    func roundCorner(_ radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func rounded() {
        let radius = min(self.frame.size.height, self.frame.size.width) / 2
        roundCorner(radius)
    }
}
