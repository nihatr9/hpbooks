//
//  BookDetailsTableViewCell.swift
//  HPBooks
//
//  Created by Nihed Majdoub on 31/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import UIKit

class BookDetailsTableViewCell: UITableViewCell {
    
    // MARK: Properties
    
    static let identifier = "BookDetailsTableViewCell"
    static let height = UITableViewAutomaticDimension
    
    // MARK: Views
    
    lazy var synopsisLabel: UILabel = {
        let view = UILabel()
        view.textAlignment = .natural
        view.lineBreakMode = .byWordWrapping
        view.numberOfLines = 0
        view.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        
        return view
    }()
    
    // MARK: Init
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupUI()
    }
    
    // MARK: UI Methods
    
    private func setupUI() {
        
        contentView.addSubview(synopsisLabel)
        
        // Auto Layout
        
        synopsisLabel.snp.makeConstraints { (make) in
            
            make.edges.equalToSuperview().inset(16)
        }
    }
    
    
}
