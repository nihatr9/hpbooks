//
//  BookCollectionViewCell.swift
//  HPBooks
//
//  Created by Nihed Majdoub on 25/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import UIKit

@objc protocol BookCollectionViewCellDelegate {
    
    func userDidTapAddButton(_ cell: BookCollectionViewCell)
}

class BookCollectionViewCell: UICollectionViewCell {
    
    static let idendifier = "BookCollectionViewCell"
    
    // MARK: Views
    
    lazy var backImageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.textAlignment = .left
        view.lineBreakMode = .byWordWrapping
        view.textColor = .white
        view.numberOfLines = 3
        view.font = UIFont.systemFont(ofSize: 18, weight: .heavy)
        
        return view
    }()
    
    private lazy var priceLabel: UILabel = {
        let view = UILabel()
        view.textAlignment = .left
        view.lineBreakMode = .byWordWrapping
        view.textColor = .white
        view.numberOfLines = 1
        view.font = UIFont.boldSystemFont(ofSize: 18)
        
        return view
    }()
    
    private lazy var addButton: UIButton = {
        let view = UIButton.makeAddToCart()
        view.addTarget(self,
                       action: #selector(addButtonTapped),
                       for: .touchUpInside)
        
        return view
    }()
    
    weak var delegate: BookCollectionViewCellDelegate?
    
    // MARK: Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupUI()
    }
    
    // MARK: UI Methods
    
    var book: Book? {
        didSet {
            guard let book = book else {return}
            
            backImageView.kf.setImage(with: URL(string: book.coverUrl))
            titleLabel.text = book.title
            priceLabel.text = "\(book.price)€"
        }
    }
    
    private func setupUI() {
        
        contentView.roundCorner(8)
        contentView.addSubview(backImageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(priceLabel)
        contentView.addSubview(addButton)
        
        // Auto Layout
        
        backImageView.snp.makeConstraints { (make) in
            
            make.edges.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { (make) in
            
            make.top.left.equalToSuperview().offset(12)
            make.right.equalToSuperview().inset(12)
        }
        
        priceLabel.snp.makeConstraints { (make) in
            
            make.left.equalTo(titleLabel)
            make.bottom.equalToSuperview().inset(12)
            make.width.equalTo(35)
        }
        
        addButton.snp.makeConstraints { (make) in
            
            make.right.equalTo(titleLabel)
            make.left.equalTo(priceLabel.snp.right).offset(20)
            make.centerY.equalTo(priceLabel)
            make.height.equalTo(30)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        addButton.rounded()
    }
    
    // MARK: add button action
    @objc
    func addButtonTapped() {
        
        UIView.animate(withDuration: 0.2, animations: {
            self.addButton.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
        }) { _ in
            UIView.animate(withDuration: 0.1, animations: {
                self.addButton.transform = CGAffineTransform.identity
            })
        }
        
        delegate?.userDidTapAddButton(self)
    }
}
