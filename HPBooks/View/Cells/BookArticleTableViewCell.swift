//
//  BookArticleTableViewCell.swift
//  HPBooks
//
//  Created by Nihed Majdoub on 30/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import UIKit
import Stepperier

protocol BookArticleTableViewCellDelegate: class {
    
    func userDidChangeBookNumber(_ cell: BookArticleTableViewCell, number: Int)
}

class BookArticleTableViewCell: UITableViewCell {
    
    // MARK: Properties
    
    static let identifier = "BookArticleTableViewCell"
    static let height: CGFloat = 80
    
    weak var delegate: BookArticleTableViewCellDelegate?
    
    // MARK: Views
    
    private lazy var coverImageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.roundCorner(8)
        
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.textAlignment = .left
        view.lineBreakMode = .byTruncatingTail
        view.numberOfLines = 1
        view.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        
        return view
    }()
    
    private lazy var priceLabel: UILabel = {
        let view = UILabel()
        view.textAlignment = .left
        view.lineBreakMode = .byWordWrapping
        view.textColor = .black
        view.numberOfLines = 1
        view.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        
        return view
    }()
    
    private let stepper: Stepperier = {
        let view = Stepperier()
        view.operationSymbolsColor = .black
        view.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        view.symbolsLineWidth = 1.5
        view.tintColor = .black
        view.valueLabelMargin = 12
        
        return view
    }()
    
    // MARK: Init
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupUI()
    }
    
    // MARK: UI Methods
    
    var articleInfo: ArticleInfo? {
        didSet {
            guard let info = articleInfo,
                let book = info.book else {return}
            
            let count = info.count
            
            coverImageView.kf.setImage(with: URL(string: book.coverUrl))
            titleLabel.text = book.title
            priceLabel.text = "\(count * book.price)€"
            stepper.value = count
        }
    }
    
    private func setupUI() {
        
        contentView.addSubview(coverImageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(stepper)
        contentView.addSubview(priceLabel)
        
        stepper.addTarget(self, action: #selector(stepperierValueDidChange(_:)), for: .valueChanged)
        
        // Auto Layout
        
        coverImageView.snp.makeConstraints { (make) in
            
            make.left.equalToSuperview().offset(12)
            make.centerY.equalToSuperview()
            make.size.equalTo(60)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            
            make.top.equalTo(coverImageView)
            make.left.equalTo(coverImageView.snp.right).offset(12)
            make.right.equalTo(priceLabel.snp.left).offset(-12)
        }
        
        stepper.snp.makeConstraints { (make) in
            
            make.bottom.equalTo(coverImageView)
            make.left.equalTo(titleLabel)
        }
        
        priceLabel.snp.makeConstraints { (make) in
            
            make.right.equalToSuperview().offset(-8)
            make.centerY.equalToSuperview()
            make.width.greaterThanOrEqualTo(50)
        }
    }
    
    // MARK: Actions
    
    @IBAction
    func stepperierValueDidChange(_ stepper: Stepperier) {
        delegate?.userDidChangeBookNumber(self, number: stepper.value)
    }
}
