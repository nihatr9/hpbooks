//
//  ShoppingCartHeader.swift
//  HPBooks
//
//  Created by Nihed Majdoub on 30/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import UIKit

class ShoppingCartHeader: UIView {
    
    // MARK: Properties
    
    static let height: CGFloat = 60
    var action: (() -> ())?
    
    // MARK: Views
    
    lazy var proceedButton: UIButton = {
        let view = UIButton(type: .system)
        view.backgroundColor = UIColor.clear
        let title = NSAttributedString(string: "proceed_button_text".localized.uppercased(),
                                       attributes: [ NSAttributedStringKey.font : UIFont.systemFont(ofSize: 18, weight: .black),
                                                     NSAttributedStringKey.foregroundColor : self.tintColor])
        view.setAttributedTitle(title, for: .normal)
        view.titleEdgeInsets = UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 4)
        view.addTarget(self, action: #selector(proceedButtonTapped), for: .touchUpInside)
        
        return view
    }()
    
    // MARK: Init
    
    init(frame: CGRect, buttonAction: @escaping () -> ()) {
        
        self.action = buttonAction
        
        super.init(frame: frame)
        
        setupUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: UI Methods
    
    private func setupUI() {
        
        addSubview(proceedButton)
        
        // Auto Layout
        
        proceedButton.snp.makeConstraints { (make) in
            
            make.center.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.65)
            make.height.equalTo(50)
        }
    }
    
    // MARK: Actions
    
    @objc
    func proceedButtonTapped() {
        action?()
    }
}
