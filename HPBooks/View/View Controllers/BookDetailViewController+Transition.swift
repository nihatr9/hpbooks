//
//  BookDetailViewController+Transition.swift
//  HPBooks
//
//  Created by Nihed Majdoub on 02/04/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import RMPZoomTransitionAnimator

extension BookDetailViewController: RMPZoomTransitionAnimating, RMPZoomTransitionDelegate {
    
    func transitionSourceImageView() -> UIImageView {
        let imageView = UIImageView(image: headerView.coverImageView.image)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.frame = headerView.coverImageView.frame
        
        return imageView
    }
    
    func transitionSourceBackgroundColor() -> UIColor {
        return UIColor.clear
    }
    
    func transitionDestinationImageViewFrame() -> CGRect {
        var frame = view.frame
        frame.size.height = 250
        
        return frame
    }
    
    func zoomTransitionAnimator(_ animator: RMPZoomTransitionAnimator, didCompleteTransition didComplete: Bool,
                                animatingSourceImageView imageView: UIImageView) {
        //        headerView.imageView.image = imageView.image
    }
}
