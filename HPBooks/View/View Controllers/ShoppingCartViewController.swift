//
//  ShoppingCartViewController.swift
//  HPBooks
//
//  Created by Nihed majdoub on 23/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import UIKit
import RealmSwift

class ShoppingCartViewController: UIViewController {
    
    // MARK: Properties
    
    var viewModel: BookListViewModel
    var bookArticlesToken: NotificationToken?
    
    // MARK: Views
    
    lazy var tableView: UITableView = {
        let view = UITableView()
        view.backgroundColor = .white
        view.delegate = self
        view.dataSource = self
        view.emptyDataSetSource = self
        view.emptyDataSetDelegate = self
        view.register(BookArticleTableViewCell.self,
                      forCellReuseIdentifier: BookArticleTableViewCell.identifier)
        view.rowHeight = BookArticleTableViewCell.height
        view.tableFooterView = UIView()
        view.allowsSelection = false
    
        return view
    }()
    
    // MARK: VC Init
    
    init(with viewModel: BookListViewModel) {
        
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: VC lifeCyle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        initRealmNotif()
    }
    
    deinit {
        bookArticlesToken?.invalidate()
    }
    
    // MARK: Helpers
    
    private func configureUI() {
        
        title = "Cart".localized
        
        let clearAllButton = UIBarButtonItem(title: "rest_text".localized,
                                             style: .done,
                                             target: self,
                                             action: #selector(clearAllTapped(_:)))
        navigationItem.setRightBarButton(clearAllButton, animated: true)
        
        view.addSubview(tableView)
        
        // Auto Layout
        tableView.snp.makeConstraints { (make) in
            
            make.edges.equalToSuperview()
        }
    }
    
    private func updateTableHeader() {
        
        if !viewModel.shoppingCart.isEmpty {
            let headerFrame = CGRect(origin: .zero,
                                     size: CGSize(width: view.bounds.width,
                                                  height: ShoppingCartHeader.height))
            let header = ShoppingCartHeader(frame: headerFrame,
                                            buttonAction: {[weak self] in
                                                
                                                self?.proceedButtonTapped()
            })
            tableView.tableHeaderView = header
        } else {
            tableView.tableHeaderView = nil
        }
    }
    
    private func initRealmNotif() {
        
        bookArticlesToken = viewModel.shoppingCart.articleInfos
            .observe { [weak self] (changes: RealmCollectionChange) in
                guard let `self` = self else { return }
                
                // remove table header when there is no articles
                self.updateTableHeader()
                
                switch changes {
                case .initial:
                    
                    self.tableView.reloadData()
                    
                case .update(_, let deletions, let insertions, let modifications):
                    
                    self.tableView.beginUpdates()
                    self.tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                              with: .automatic)
                    self.tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                              with: .automatic)
                    self.tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                              with: .none)
                    self.tableView.endUpdates()
                    
                case .error(let error):
                    fatalError("\(error)")
                }
        }
    }
    
    // MARK: Action
    
    @objc
    func clearAllTapped(_ sender: UIBarButtonItem) {
        
        showActionVC(title: "rest_title".localized,
                     message: "rest_message".localized,
                     actionText: "rest_text".localized,
                     source: sender) {[weak self] in
                        
                        self?.viewModel.libraryService.resetShoppingCart()
        }
    }
    
    func proceedButtonTapped() {
        
        viewModel.libraryService.booksOffer(with: viewModel.shoppingCart.booksIds) {[weak self] (error) in
            guard let `self` = self else {return}
            
            if let error = error {
                self.showSipmleAlertVC(title: "Error", message: error, handler: nil)
                
            } else {
                let vc = CheckOutViewController(with: self.viewModel)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}

extension ShoppingCartViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.shoppingCart.articleInfos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView
            .dequeueReusableCell(withIdentifier: BookArticleTableViewCell.identifier,
                                 for: indexPath) as! BookArticleTableViewCell
        let info = viewModel.article(at: indexPath.row)
        
        cell.articleInfo = info
        cell.delegate = self
        
        return cell
    }
}

extension ShoppingCartViewController: BookArticleTableViewCellDelegate {
    
    func userDidChangeBookNumber(_ cell: BookArticleTableViewCell, number: Int) {
        guard let info = cell.articleInfo, let book = info.book else {return}
        
        // Plus button tapped -> add book
        if info.count < number {
            viewModel.libraryService.addBookToShoppingCart(book)
            
            // Minus button tapped -> remove book
        } else {
            viewModel.libraryService.removeBookFromShoppingCart(book)
        }
    }
}
