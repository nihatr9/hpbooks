//
//  BookListViewController.swift
//  HPBooks
//
//  Created by Nihed majdoub on 23/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import UIKit
import RealmSwift
import SnapKit
import Kingfisher
import BBBadgeBarButtonItem

let isIpad = UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad

class BookListViewController: UIViewController {
    
    // MARK: Views
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: self.view.frame.size.width / (isIpad ? 4 : 2) - 16,
                                 height: 160)
        layout.minimumLineSpacing = 8
        layout.minimumInteritemSpacing = 8
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.backgroundColor = .white
        view.contentInset = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
        view.register(BookCollectionViewCell.self,
                      forCellWithReuseIdentifier: BookCollectionViewCell.idendifier)
        view.delegate = self
        view.dataSource = self
        view.allowsMultipleSelection = false
        
        return view
    }()
    
    lazy var cartBarButton: BBBadgeBarButtonItem? = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "cart"), for: [])
        button.addTarget(self,
                         action: #selector(cartButtonTapped),
                         for: .touchUpInside)
        
        let view = BBBadgeBarButtonItem(customUIButton: button)
        view?.badgeOriginX = 20
        view?.badgeOriginY = -4
        
        return view
    }()
    
    // MARK: Properties

    var booksToken: NotificationToken?
    var articlesToken: NotificationToken?
    
    let viewModel = BookListViewModel(with: LibraryService())

    // MARK: VC lifeCyle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        initRealmNotif()
    }
    
    deinit {
        booksToken?.invalidate()
        articlesToken?.invalidate()
    }
    
    // MARK: Helpers
    
    private func configureUI() {
        
        if(traitCollection.forceTouchCapability == .available) {
            
            registerForPreviewing(with: self, sourceView: collectionView)
        }
        
        title = "HP Books".localized
        navigationItem.setRightBarButton(cartBarButton, animated: true)
        navigationController?.delegate = self
        
        view.addSubview(collectionView)
        
        // Auto Layout
        collectionView.snp.makeConstraints { (make) in
            
            make.edges.equalToSuperview()
        }
    }
    
    private func updateBadgeValue() {
        
        cartBarButton?.badgeValue = "\(viewModel.libraryService.shoppingCart.booksCount)"
    }
    
    private func initRealmNotif() {

        booksToken = viewModel.books.observe({ [weak self] changes in
            guard let collectionView = self?.collectionView else {return}

            switch changes {
            case .initial, .update(_, _, _, _):
                collectionView.reloadData()
            case .error(let error):
                fatalError("\(error)")
            }
        })

        articlesToken = viewModel.libraryService.shoppingCart.articleInfos.observe({[weak self] (change) in
            self?.updateBadgeValue()
        })
    }
    
    // MARK: Actions
    
    @objc
    func cartButtonTapped() {
        
        let vc = ShoppingCartViewController(with: viewModel)
        navigationController?.pushViewController(vc, animated: true)
    }
}

// MARk: UICollectionView Delegate & DataSource

extension BookListViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return viewModel.books.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView
            .dequeueReusableCell(withReuseIdentifier: BookCollectionViewCell.idendifier,
                                 for: indexPath) as! BookCollectionViewCell
        
        let book = viewModel.book(at: indexPath.row)
        cell.book = book
        cell.delegate = self
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let book = viewModel.book(at: indexPath.row)
        let vc = BookDetailViewController(with: BookDetailsViewModel(libraryService: viewModel.libraryService,
                                                                     book: book))
        navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: BookCollectionViewCell Delegate

extension BookListViewController: BookCollectionViewCellDelegate {
    
    func userDidTapAddButton(_ cell: BookCollectionViewCell) {
        guard let book = cell.book else {return}
        
        viewModel.libraryService.addBookToShoppingCart(book)
    }
}

// MARK: UIViewControllerPreviewing Delegate

extension BookListViewController: UIViewControllerPreviewingDelegate {
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing,
                           viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = collectionView.indexPathForItem(at: location),
            let cell = collectionView.cellForItem(at: indexPath) as? BookCollectionViewCell else { return nil }
        
        let book = viewModel.book(at: indexPath.row)
        let vc = BookDetailViewController(with: BookDetailsViewModel(libraryService: viewModel.libraryService,
                                                                     book: book))
        
        vc.preferredContentSize = CGSize(width: 0.0, height: view.bounds.height * 0.5)
        previewingContext.sourceRect = cell.frame
        
        return vc
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing,
                           commit viewControllerToCommit: UIViewController) {
        if viewControllerToCommit is BookDetailViewController {
            navigationController?.pushViewController(viewControllerToCommit, animated: true)
        }
    }
}
