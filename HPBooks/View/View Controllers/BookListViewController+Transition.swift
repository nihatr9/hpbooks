//
//  BookListViewController+Transition.swift
//  HPBooks
//
//  Created by Nihed Majdoub on 02/04/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import RMPZoomTransitionAnimator

extension BookListViewController:  UINavigationControllerDelegate , RMPZoomTransitionAnimating, RMPZoomTransitionDelegate {
    
    func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationControllerOperation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard let fromVC = fromVC as? RMPZoomTransitionAnimating & RMPZoomTransitionDelegate ,
            let toVC = toVC as? RMPZoomTransitionAnimating & RMPZoomTransitionDelegate else {return nil}
        
        let animator = RMPZoomTransitionAnimator()
        animator.goingForward = operation == .push
        animator.sourceTransition = fromVC
        animator.destinationTransition = toVC
        
        return animator
    }
    
    func transitionSourceImageView() -> UIImageView {
        guard let index = collectionView.indexPathsForSelectedItems?.first,
            let attributes = collectionView.layoutAttributesForItem(at: index),
            let cell = collectionView.cellForItem(at: index) as? BookCollectionViewCell else {return UIImageView()}
        
        let imageView = UIImageView(image: cell.backImageView.image)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = false
        let frame = collectionView.convert(attributes.frame, to: view)
        imageView.frame = frame
        
        return imageView
    }
    
    func transitionSourceBackgroundColor() -> UIColor {
        return UIColor.clear
    }
    
    func transitionDestinationImageViewFrame() -> CGRect {
        guard let index = collectionView.indexPathsForSelectedItems?.first,
            let attributes = collectionView.layoutAttributesForItem(at: index) else {return CGRect.zero}

        let frame = collectionView.convert(attributes.frame, to: view)
        
        return frame
    }
}
