//
//  ShoppingCartViewController+EmptyState.swift
//  HPBooks
//
//  Created by Nihed Majdoub on 03/04/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import DZNEmptyDataSet

extension ShoppingCartViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "empty_cart")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "empty_cart_text".localized,
                                  attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 18,
                                                                                              weight: .heavy)])
    }
}
