//
//  BookDetailViewController.swift
//  HPBooks
//
//  Created by Nihed Majdoub on 28/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import UIKit
import ParallaxHeader

class BookDetailViewController: UIViewController {
    
    // MARK: Properties
//    private let book: Book
//    private let libraryService: LibraryService

    var viewModel: BookDetailsViewModel

    // MARK: Views
    
    lazy var headerView: HeaderView = {
        let view = HeaderView(with: viewModel.book.coverUrl,
                              price: viewModel.book.price,
                              action: {[weak self] in
                                self?.addButtonTapped()
        })
        
        return view
    }()
    
    lazy var tableView: UITableView = {
        let view = UITableView()
        view.backgroundColor = .white
        view.delegate = self
        view.dataSource = self
        view.register(BookDetailsTableViewCell.self,
                      forCellReuseIdentifier: BookDetailsTableViewCell.identifier)
        view.rowHeight = BookDetailsTableViewCell.height
        view.estimatedRowHeight = 250
        view.tableFooterView = UIView()
        view.allowsSelection = false
        view.separatorStyle = .none
        view.contentInsetAdjustmentBehavior  = .always
        
        return view
    }()
    
    // MARK: VC Init
    
    init(with viewModel: BookDetailsViewModel) {
        
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: VC lifeCyle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
    
    // MARK: Helpers
    
    private func configureUI() {
        
        title = viewModel.book.title
        
        view.addSubview(tableView)
        
        // Add parallax animation
        tableView.parallaxHeader.view = headerView
        tableView.parallaxHeader.height = 300
        tableView.parallaxHeader.minimumHeight = 0
        tableView.parallaxHeader.mode = .centerFill
        
        // Auto Layout
        tableView.snp.makeConstraints { (make) in
            
            make.edges.equalToSuperview().inset(view.safeAreaInsets)
        }
    }
    
    // MARK: Actions
    
    @objc
    func addButtonTapped() {
        
        viewModel.libraryService.addBookToShoppingCart(viewModel.book)
        showSipmleAlertVC(title: "", message: "The book has been added successfully", handler: nil)
    }
}

extension BookDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView
            .dequeueReusableCell(withIdentifier: BookDetailsTableViewCell.identifier,
                                 for: indexPath) as! BookDetailsTableViewCell
        cell.synopsisLabel.text = viewModel.book.synopsis
        
        return cell
    }
}

// MARK: Header View

class HeaderView: UIView {
    
    private lazy var addButton: UIButton = {
        let view = UIButton.makeAddToCart()
        view.addTarget(self,
                       action: #selector(addButtonTapped),
                       for: .touchUpInside)
        
        return view
    }()
    
    lazy var coverImageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.kf.setImage(with: URL(string: urlString))
        
        return view
    }()
    
    private lazy var priceLabel: UILabel = {
        let view = UILabel()
        view.textAlignment = .left
        view.lineBreakMode = .byWordWrapping
        view.textColor = .white
        view.numberOfLines = 1
        view.font = UIFont.boldSystemFont(ofSize: 22)
        view.text = "\(price)€"
        
        return view
    }()
    
    var action: (() -> ())?
    var urlString = ""
    var price = 0
    
    // MARK: Init
    
    init(with urlString: String, price: Int, action: (() -> ())?) {
        
        self.action = action
        self.urlString = urlString
        self.price = price
        
        super.init(frame: .zero)
        
        setupUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: UI Methods
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        addButton.rounded()
    }
    
    private func setupUI() {
        
        addSubview(coverImageView)
        addSubview(addButton)
        addSubview(priceLabel)
        
        // Auto Layout
        
        coverImageView.snp.makeConstraints { (make) in
            
            make.edges.equalToSuperview()
        }
        
        addButton.snp.makeConstraints { (make) in
            
            make.height.equalTo(35)
            make.width.equalTo(90)
            make.bottom.equalToSuperview().inset(20)
            make.right.equalToSuperview().inset(20)
        }
        
        priceLabel.snp.makeConstraints { (make) in
            
            make.left.equalToSuperview().offset(20)
            make.centerY.equalTo(addButton)
        }
    }
    
    // MARK: Actions
    
    @objc
    func addButtonTapped() {
        
        UIView.animate(withDuration: 0.2, animations: {
            self.addButton.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
        }) { _ in
            UIView.animate(withDuration: 0.1, animations: {
                self.addButton.transform = CGAffineTransform.identity
            })
        }
        
        action?()
    }
}

