//
//  CheckOutViewController.swift
//  HPBooks
//
//  Created by Nihed Majdoub on 30/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import UIKit
import BUYPaymentButton

class CheckOutViewController: UIViewController {
    
    // MARK: Views
    
    private lazy var totalPricelabel: UILabel = {
        let view = UILabel()
        view.textAlignment = .center
        view.lineBreakMode = .byWordWrapping
        view.textColor = .gray
        view.numberOfLines = 1
        view.font = UIFont.systemFont(ofSize: 20, weight: .heavy)
        view.text = "total_price_text".localized + " \(viewModel.shoppingCart.totalPrice)€"
        
        return view
    }()
    
    private lazy var discountlabel: UILabel = {
        let view = UILabel()
        view.textAlignment = .center
        view.lineBreakMode = .byWordWrapping
        view.textColor = .gray
        view.numberOfLines = 1
        view.font = UIFont.systemFont(ofSize: 20, weight: .heavy)
        let discount = Double(viewModel.shoppingCart.totalPrice) - viewModel.shoppingCart.finalPrice
        view.text = "discount_text".localized + " -\(discount)€"
        
        return view
    }()
    
    private lazy var paymentlabel: UILabel = {
        let view = UILabel()
        view.textAlignment = .center
        view.lineBreakMode = .byWordWrapping
        view.textColor = .gray
        view.numberOfLines = 1
        view.font = UIFont.systemFont(ofSize: 20, weight: .heavy)
        view.text = "payment_price_text".localized + " \(viewModel.shoppingCart.finalPrice)€"
        
        return view
    }()
    
    lazy var payButton: BUYPaymentButton = {
        let view = BUYPaymentButton(type: BUYPaymentButtonType.buy,
                                    style: BUYPaymentButtonStyle.black)!
        view.addTarget(self, action: #selector(payButtonTapped),
                       for: .touchUpInside)
        
        return view
    }()
    
    // MARK: Properties
    
    var viewModel: BookListViewModel
    
    // MARK: VC Init
    
    init(with viewModel: BookListViewModel) {
        
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: VC lifeCyle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
    
    // MARK: Helpers
    
    private func configureUI() {
        
        
        title = "Order summary".localized
        
        view.backgroundColor = .white
        
        view.addSubview(totalPricelabel)
        view.addSubview(discountlabel)
        view.addSubview(paymentlabel)
        view.addSubview(payButton)
        
        totalPricelabel.snp.makeConstraints { (make) in
            
            make.bottom.equalTo(view.snp.centerY)
            make.centerX.equalToSuperview()
        }
        
        discountlabel.snp.makeConstraints { (make) in
            
            make.centerX.equalToSuperview()
            make.top.equalTo(totalPricelabel.snp.bottom).offset(12)
        }
        
        paymentlabel.snp.makeConstraints { (make) in
            
            make.centerX.equalToSuperview()
            make.top.equalTo(discountlabel.snp.bottom).offset(12)
        }
        
        payButton.snp.makeConstraints { (make) in
            
            make.centerX.equalToSuperview()
            make.top.equalTo(paymentlabel.snp.bottom).offset(30)
            make.width.equalToSuperview().multipliedBy(0.65)
            make.height.equalTo(50)
        }
    }
    
    // MARK: Actions
    
    @objc
    func payButtonTapped() {
        
        showSipmleAlertVC(title: "Congratulations",
                          message: "your order transaction was successful") {[weak self] in
                            
                            self?.viewModel.libraryService.resetShoppingCart()
                            self?.navigationController?.popToRootViewController(animated: true)
        }
    }
    
}
