//
//  CartViewModel.swift
//  HPBooks
//
//  Created by Nihed Majdoub on 03/04/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import RealmSwift

struct BookListViewModel {
    
    // MARK: Properties

    var libraryService: LibraryService
    var books: Results<Book>

    lazy var shoppingCart: ShoppingCart = {
        return libraryService.shoppingCart
    }()
    
    // MARK: Init
    
    init(with apiService: LibraryService) {
        self.libraryService = apiService
        books = libraryService.allBooks()
    }
    
    // MARK: Helpers
    
    func book(at index: Int) -> Book {
        return books[index]
    }
    
    func article(at index: Int) -> ArticleInfo {
        return libraryService.shoppingCart.articleInfos[index]
    }
}

struct BookDetailsViewModel {
    
    var libraryService: LibraryService
    var book: Book
}
