//
//  LibraryService.swift
//  HPBooks
//
//  Created by Nihed majdoub on 23/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import RealmSwift

typealias MessageCompletion = ((_ errorMessage: String?) -> Void)

class LibraryService {
    
    private let apiType: ApiServiceProtocol.Type
    private let realmService: RealmServiceProtocol
    
    // MARK: Init
    init(apiType: ApiServiceProtocol.Type = ApiService.self,
        realmService: RealmServiceProtocol = RealmService()) {
        
        self.apiType = apiType
        self.realmService = realmService
    }
    
    // MARK: Methods
    
    // Books
    
    func allBooks(_ completion: MessageCompletion? = nil) -> Results<Book> {
        
        let books = realmService.allBookFromDB()
        
        apiType.allBooksApi {[weak self] (result) in
            switch result {
            case .success(let jsonArray):
                self?.realmService.saveBooks(with: jsonArray)
                completion?(nil)
            case .failure(let error):
                completion?(ErrorManager.errorMessage(from: error))
            }
        }
        
        return books
    }
    
    func booksOffer(with ids: [String], completion: MessageCompletion? = nil) {
        
        apiType.booksOffersApi(with: ids) {[weak self] (result) in
            switch result {
            case .success(let jsonDict):
                self?.realmService.updateShoppingCartOffers(with: jsonDict)
                completion?(nil)
            case .failure(let error):
                completion?(ErrorManager.errorMessage(from: error))
            }
        }
    }
    
    // Shopping Cart

    var shoppingCart: ShoppingCart {
        return realmService.getShoppingCart()
    }
    
    func addBookToShoppingCart(_ book: Book) {
        realmService.addBookToShoppingCart(book)
    }
    
    func removeBookFromShoppingCart(_ book: Book) {
        realmService.removeBookFromShoppingCart(book)
    }
    
    func resetShoppingCart() {
        realmService.resetShoppingCart()
    }
}
