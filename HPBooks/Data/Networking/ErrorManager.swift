//
//  ErrorManager.swift
//  HPBooks
//
//  Created by Nihed majdoub on 27/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

struct ErrorManager {
    
    static func errorMessage(from error: ApiError) -> String {
        
        switch error {
        case .invaLidDataResponse:
            return "InvaLid Data Response"
        case .networkUnreachable:
            return "Network Unreachable"
        case .requestFailed:
            return "Request Failed"
        }
    }
}
