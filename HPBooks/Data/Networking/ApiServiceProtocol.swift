//
//  ApiServiceProtocol.swift
//  HPBooks
//
//  Created by Nihed Majdoub on 31/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

typealias ApiServiceProtocol = BooksApiProtocol & OffersApiProtocol

protocol BooksApiProtocol {
    
    static func allBooksApi(completion: @escaping ResponseArrayHandler)
}

protocol OffersApiProtocol {
    
    static func booksOffersApi(with bookIds: [String],
                               completion: @escaping ResponseDictionaryHandler)
}
