//
//  ApiService.swift
//  HPBooks
//
//  Created by Nihed majdoub on 23/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import Foundation
import Alamofire

typealias JSONDictionary = [String : Any]
typealias JSONArray = [JSONDictionary]

typealias ResponseDictionaryHandler = (Result<JSONDictionary>) -> ()
typealias ResponseArrayHandler = (Result<JSONArray>) -> ()

enum Result<T> {
    
    case success(T)
    case failure(ApiError)
}

enum ApiError: Error {
    
    case requestFailed
    case invaLidDataResponse
    case networkUnreachable
}

struct ApiService {
    
    fileprivate enum URLProvider: String {
        
        case books = "/books"
        
        private var baseURL: String { return "http://henri-potier.xebia.fr" }
        
        var url: URL { return URL(string: baseURL.appending(rawValue))! }
        
        static func offersUrl(with bookIds: [String]) -> URL {
            
            let ids = bookIds.joined(separator: ",")
            let booksUrlString = URLProvider.books.url.absoluteString
            return URL(string: booksUrlString.appendingFormat("/%@/commercialOffers", ids))!
        }
    }
}

// MARK: Books Api conformance

extension ApiService: BooksApiProtocol {
    
    // get all books from backend
    static func allBooksApi(completion: @escaping ResponseArrayHandler) {
        
        Alamofire.request(URLProvider.books.url, method: .get)
            .validate()
            .responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    completion(Result.failure(ApiError.requestFailed))
                    return
                }
                
                guard let jsonArray = response.value as? JSONArray else {
                    completion(Result.failure(ApiError.invaLidDataResponse))
                    return
                }
                
                completion(Result.success(jsonArray))
        }
    }
}

// MARK: Book's Offer Api conformance

extension ApiService: OffersApiProtocol {
    
    static func booksOffersApi(with bookIds: [String],
                               completion: @escaping ResponseDictionaryHandler) {
        
        Alamofire.request(URLProvider.offersUrl(with: bookIds), method: .get)
            .validate()
            .responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    completion(Result.failure(ApiError.requestFailed))
                    return
                }
                
                guard let jsonDict = response.value as? JSONDictionary else {
                    completion(Result.failure(ApiError.invaLidDataResponse))
                    return
                }
                
                completion(Result.success(jsonDict))
        }
    }
}
