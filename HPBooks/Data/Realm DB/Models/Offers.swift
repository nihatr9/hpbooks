//
//  Offers.swift
//  HPBooks
//
//  Created by Nihed Majdoub on 23/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import RealmSwift
import ObjectMapper

class OfferItem: Object, Mappable {
    
    // MARK: Properties
    @objc dynamic var type = ""
    @objc dynamic var value: Double = 0
    @objc dynamic var sliceValue: Double = 0
    
    // MARK: Meta
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        type <- map["type"]
        value <- map["value"]
        sliceValue <- map["sliceValue"]
    }
}

class Offers: Object, Mappable {
    
    // MARK: - Properties
    @objc dynamic var id = NSUUID().uuidString
    var items = List<OfferItem>()
    
    // MARK: - Meta
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    static func mappedOffers(with model: JSONDictionary) -> Offers {
        let offers = Mapper<Offers>().map(JSON: model)! as Offers
        
        return offers
    }
    
    func mapping(map: Map) {
        
        items <- ( map["offers"], ListTransform<OfferItem>())
    }
}
