//
//  ShoppingCart.swift
//  HPBooks
//
//  Created by Nihed Majdoub on 23/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import RealmSwift

class ArticleInfo: Object {
    
    // MARK: Properties
    @objc dynamic var book: Book?
    @objc dynamic var count = 1
    
    // MARK: Helper
    
    static func articleInfo(with book: Book) -> ArticleInfo {
        let info = ArticleInfo()
        info.book = book
        
        return info
    }
}

class ShoppingCart: Object {
    
    // MARK: Properties
    @objc dynamic var id = "Xebia"
    @objc dynamic var offers: Offers?
    
    var articleInfos = List<ArticleInfo>()
    
    // MARK: Meta
    override static func primaryKey() -> String? {
        return "id"
    }
}

// MARK: Article Infos Helpers

extension ShoppingCart {
    
    private var books: [Book] {
        return articleInfos.compactMap({$0.book})
    }
    
    var booksIds: [String] {
        return books.map({$0.isbn})
    }
    
    var booksCount: Int {
        return articleInfos.map({$0.count}).reduce(0, +)
    }
    
    private func articleInfo(with book: Book) -> ArticleInfo? {
        return articleInfos.filter("book = %@", book).first
    }
    
    func bookIsInclude(_ book: Book) -> Bool {
        return books.contains(book)
    }
    
    func addArticleInfo(with book : Book) {
        
        let info = ArticleInfo.articleInfo(with: book)
        articleInfos.append(info)
    }
    
    func removeArticleInfo(with book : Book) {
        guard let info = articleInfo(with: book),
            let index = articleInfos.index(of: info) else {return}
        
        if info.count == 1 {
            articleInfos.remove(at: index)
        } else {
            info.count -= 1
        }
    }
    
    func updateBookCount(_ book: Book) {
        let info = articleInfo(with: book)
        info?.count += 1
    }
    
    var isEmpty: Bool {
        return articleInfos.count == 0
    }
}

// MARK: Book price

extension ShoppingCart {
    
    var totalPrice: Int {
        
        return articleInfos.map({ (info) -> Int in
            guard let book = info.book else {return 0}
            
            return book.price * info.count
        }).reduce(0, +)
    }
    
    var finalPrice: Double {
        return bestOffer(with: Double(totalPrice))
    }
    
    // Helpers
    // get best price from offers
    private func bestOffer(with totalPrice: Double) -> Double {
        
        var finalPrice = totalPrice
        
        guard  let offers = offers else { return finalPrice }
        
        for offer in offers.items {
            
            switch offer.type {
            case "percentage":
                let newPrice = totalPrice - (totalPrice * offer.value / 100)
                finalPrice = min(finalPrice, newPrice)
            case "minus":
                let newPrice = totalPrice - offer.value
                finalPrice = min(finalPrice, newPrice)
            case "slice":
                // Sanity check
                guard offer.sliceValue > 0 else {break}
                
                let multiplier = Int(totalPrice / offer.sliceValue)
                
                let newPrice = totalPrice - (Double(multiplier) * offer.value)
                finalPrice = min(finalPrice, newPrice)
            default:break
            }
        }
        
        return finalPrice
    }
}
