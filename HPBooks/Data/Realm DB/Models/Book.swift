//
//  Book.swift
//  HPBooks
//
//  Created by Nihed majdoub on 23/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import RealmSwift
import ObjectMapper

class Book: Object {
    
    // MARK: Properties
    @objc dynamic var isbn = ""
    @objc dynamic var title = ""
    @objc dynamic var price = 0
    @objc dynamic var coverUrl = ""
    @objc dynamic var synopsis: String?
    
    // MARK: Meta
    override static func primaryKey() -> String? {
        return "isbn"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
}

extension Book: Mappable {
    
    static func mappedBook(with model: JSONDictionary) -> Book {
        let book = Mapper<Book>().map(JSON: model)! as Book
        
        return book
    }

    func mapping(map: Map) {
        
        isbn <- map["isbn"]
        title <- map["title"]
        price <- map["price"]
        coverUrl <- map["cover"]
        
        let textArray = map["synopsis"].currentValue as? [String]
        synopsis = textArray?.joined(separator: "\n\n")
    }
}
