//
//  RealmService.swift
//  HPBooks
//
//  Created by Nihed majdoub on 23/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import RealmSwift

struct RealmService: RealmServiceProtocol {
    
    init(with config: Realm.Configuration = Realm.Configuration.defaultConfiguration) {
        
        Realm.Configuration.defaultConfiguration = config
        
        do {
            let _ = try Realm()
        } catch {
            fatalError("Init Realm Error")
        }
        
        // create a new shopping cart if one doesn't already exists
        getShoppingCart()
    }
    
    // MARK: Books
    
    // get all books from Realm DataBase
    func allBookFromDB() -> Results<Book> {
        let realm = try! Realm()
        return realm.objects(Book.self)
    }
    
    // save books in Realm DataBase
    func saveBooks(with jsonArray: JSONArray) {
        let realm = try! Realm()
        try! realm.write {
            
            for model in jsonArray {
                let bookModel = Book.mappedBook(with: model)
                realm.add(bookModel, update: true)
            }
        }
    }
    
    // MARK: Shoppping cart
    
    // Return the existant shopping cart or create and retrun a new one
    @discardableResult
    func getShoppingCart() -> ShoppingCart {
        let realm = try! Realm()
        
        // cart does exist
        if let cart = realm.objects(ShoppingCart.self).first {
            return cart
        }
        
        return addShoppingCart()
    }
    
    func updateShoppingCartOffers(with model: JSONDictionary) {
        
        let cart = getShoppingCart()
        let realm = try! Realm()
        try! realm.write {
            cart.offers = Offers.mappedOffers(with: model)
        }
    }
    
    func addBookToShoppingCart(_ book: Book) {
        
        let cart = getShoppingCart()
        
        let realm = try! Realm()
        try! realm.write {
            // increment count
            if cart.bookIsInclude(book) {
                
                cart.updateBookCount(book)
                
                // add new article info
            } else {
                
                cart.addArticleInfo(with: book)
            }
        }
    }
    
    func removeBookFromShoppingCart(_ book: Book) {
        let cart = getShoppingCart()
        
        let realm = try! Realm()
        try! realm.write {
            
            cart.removeArticleInfo(with: book)
        }
    }
    
    func resetShoppingCart() {
        
        let cart = getShoppingCart()
        let realm = try! Realm()
        try! realm.write {
            cart.offers = nil
            cart.articleInfos.removeAll()
        }
    }
    
    // MARK: Private Helpers
    
    // Add shoppping cart model
    @discardableResult
    private func addShoppingCart() -> ShoppingCart {
        
        let realm = try! Realm()
        let shoppingCart = ShoppingCart()
        try! realm.write {
            
            realm.add(shoppingCart, update: true)
        }
        
        return shoppingCart
    }
}
