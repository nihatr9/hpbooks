//
//  RealmServiceProtocol.swift
//  HPBooks
//
//  Created by Nihed Majdoub on 31/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import RealmSwift

protocol RealmServiceProtocol {
    
    // MARK: Books Realm methods
    func allBookFromDB() -> Results<Book>
    func saveBooks(with jsonArray: JSONArray)
    
    // MARK: ShoppingCart Realm methods
    func getShoppingCart() -> ShoppingCart
    func updateShoppingCartOffers(with model: JSONDictionary)
    func addBookToShoppingCart(_ book: Book)
    func removeBookFromShoppingCart(_ book: Book)
    func resetShoppingCart()
}
