//
//  Mocks.swift
//  HPBooksTests
//
//  Created by Nihed majdoub on 27/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import ObjectMapper
import RealmSwift
@testable import HPBooks

struct MockProvider {
    
    static let realm = try! Realm()
    
    static var firstBook: Book {
        
        let book = Book(JSON: ["isbn" : "c8fabf68-8374-48fe-a7ea-a00ccd07afff",
                               "title" : "Henri Potier à l'école des sorciers",
                               "price" : 35,
                               "cover" : "http://henri-potier.xebia.fr/hp0.jpg",
                               "synopsis" : "Xebia is awesome"])!
        
        try! realm.write {
            realm.add(book, update: true)
        }
        
        return book
    }
    
    static var secondBook: Book {
        
        let book = Book(JSON: ["isbn" : "a460afed-e5e7-4e39-a39d-c885c05db861",
                               "title" : "Henri Potier et la Chambre des secrets",
                               "price" : 30,
                               "cover" : "http://henri-potier.xebia.fr/hp1.jpg",
                               "synopsis" : "Xebia is awesome"])!
        
        try! realm.write {
            realm.add(book, update: true)
        }
        
        return book
    }
    
    static var testBook: Book {
        
        let book = Book(JSON: ["isbn" : "fcd1e6fa-a63f-4f75-9da4-b560020b6acc",
                               "title" : "Henri Potier et le Prisonnier d'Azkaban",
                               "price" : 300,
                               "cover" : "http://henri-potier.xebia.fr/hp2.jpg",
                               "synopsis" : "Xebia is awesome"])!
        
        try! realm.write {
            realm.add(book, update: true)
        }
        
        return book
    }
}
