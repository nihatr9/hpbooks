//
//  HPBooksTests.swift
//  HPBooksTests
//
//  Created by Nihed majdoub on 23/03/2018.
//  Copyright © 2018 Nihed majdoub. All rights reserved.
//

import Quick
import Nimble
import RealmSwift
@testable import HPBooks

class HPBooksTests: QuickSpec {
    
    lazy var librayService: LibraryService = {
        var config = Realm.Configuration.defaultConfiguration
        config.inMemoryIdentifier = "memory"
        config.deleteRealmIfMigrationNeeded = true
        
        
        return LibraryService(apiType: ApiService.self, realmService: RealmService(with: config))
    }()
    
    override func spec() {
        
        afterEach {
            let realm = try! Realm()
            try! realm.write {
                realm.deleteAll()
            }
            
        }
        
        describe("Library Service") {
            
            context("fetching Books from Backend", closure: {
                it("should have 7 books", closure: {
                    
                    var books: Results<Book>?
                    waitUntil(timeout: 5, action: { (done) in
                        books = self.librayService.allBooks { (error) in
                            
                            expect(error).to(beNil())
                            expect(books?.count) == 7
                            done()
                        }
                    })
                })
                
                it("should have isbn, name, price, coverUrl and synopsis", closure: {
                    var books: Results<Book>?
                    waitUntil(timeout: 5, action: { (done) in
                        books = self.librayService.allBooks { (error) in
                            
                            done()
                        }
                    })
                    
                    let book = books?.first
                    expect(book?.isbn) == "c8fabf68-8374-48fe-a7ea-a00ccd07afff"
                    expect(book?.title) == "Henri Potier à l'école des sorciers"
                    expect(book?.price) == 35
                    expect(book?.coverUrl) == "http://henri-potier.xebia.fr/hp0.jpg"
                    expect(book?.synopsis?.count) > 0
                })
            })
            
            context("fetching book offers", closure: {
                it("shopping cart hould have offers with 3 offer items", closure: {
                    
                    let shoppingCart = self.librayService.shoppingCart
                    let firstBook = MockProvider.firstBook
                    let secondBook = MockProvider.secondBook
                    
                    self.librayService.addBookToShoppingCart(firstBook)
                    self.librayService.addBookToShoppingCart(secondBook)
                    
                    waitUntil(timeout: 5, action: { (done) in
                        self.librayService.booksOffer(with: shoppingCart.booksIds, completion: { (error) in
                            
                            expect(error).to(beNil())
                            done()
                        })
                    })
                    
                    expect(shoppingCart.offers?.items.count) > 0
                })
            })
            
            context("Adding 2 different books to Shopping cart", closure: {
                it("should have 2 books", closure: {
                    
                    let shoppingCart = self.librayService.shoppingCart
                    let firstBook = MockProvider.firstBook
                    let secondBook = MockProvider.secondBook
                    
                    self.librayService.addBookToShoppingCart(firstBook)
                    self.librayService.addBookToShoppingCart(secondBook)
                    
                    expect(shoppingCart.booksCount) == 2
                    expect(shoppingCart.articleInfos.first?.book) == firstBook
                    expect(shoppingCart.articleInfos.last?.book) == secondBook
                })
            })
            
            context("Adding the same book twice  to Shopping cart", closure: {
                it("should have only 1 article info and 2 books", closure: {
                    
                    let shoppingCart = self.librayService.shoppingCart
                    let firstBook = MockProvider.firstBook
                    
                    self.librayService.addBookToShoppingCart(firstBook)
                    self.librayService.addBookToShoppingCart(firstBook)
                    
                    expect(shoppingCart.articleInfos.count) == 1

                    expect(shoppingCart.booksCount) == 2
                    expect(shoppingCart.articleInfos.first?.count) == 2
                    expect(shoppingCart.articleInfos.first?.book) == firstBook

                })
            })
            
            context("Removing book from Shopping cart", closure: {
                it("should have 0 books", closure: {
                    let shoppingCart = self.librayService.shoppingCart
                    let book = MockProvider.firstBook
                    
                    self.librayService.addBookToShoppingCart(book)
                    self.librayService.removeBookFromShoppingCart(book)
                    
                    expect(shoppingCart.booksCount) == 0
                })
            })
            
            context("Resetting Shopping cart", closure: {
                it("should have 0 books & 0 offers", closure: {
                    
                    let shoppingCart = self.librayService.shoppingCart
                    let book = MockProvider.firstBook
                    self.librayService.addBookToShoppingCart(book)

                    waitUntil(timeout: 5, action: { (done) in
                        self.librayService.booksOffer(with: shoppingCart.booksIds, completion: { (error) in
                            
                            done()
                        })
                    })
                    
                    
                    expect(shoppingCart.booksCount) == 1
                    expect(shoppingCart.offers?.items.count) > 0
                    
                    self.librayService.resetShoppingCart()
                    expect(shoppingCart.isEmpty).to(beTrue())
                    expect(shoppingCart.offers).to(beNil())
                })
            })
            
            context("Shopping cart with minus as best offer ", closure: {
                it("should be 50€", closure: {
                    
                    let shoppingCart = self.librayService.shoppingCart
                    let firstBook = MockProvider.firstBook
                    let secondBook = MockProvider.secondBook
                    
                    self.librayService.addBookToShoppingCart(firstBook)
                    self.librayService.addBookToShoppingCart(secondBook)
                    
                    waitUntil(timeout: 5, action: { (done) in
                        self.librayService.booksOffer(with: shoppingCart.booksIds, completion: { (error) in
                            
                            done()
                        })
                    })
                    
                    expect(shoppingCart.finalPrice) == 50
                })
            })
            
            context("Shopping cart with percentage as best offer ", closure: {
                it("should be 33.6€", closure: {
                    
                    let shoppingCart = self.librayService.shoppingCart
                    let firstBook = MockProvider.firstBook
                    
                    self.librayService.addBookToShoppingCart(firstBook)
                    
                    waitUntil(timeout: 5, action: { (done) in
                        self.librayService.booksOffer(with: shoppingCart.booksIds, completion: { (error) in
                            
                            done()
                        })
                    })
                    
                    expect(shoppingCart.finalPrice) == 33.6
                })
            })
            
            context("Shopping cart with slice as best offer ", closure: {
                it("should be 329€", closure: {
                    
                    let shoppingCart = self.librayService.shoppingCart
                    let firstBook = MockProvider.firstBook
                    let secondBook = MockProvider.secondBook
                    let testBook = MockProvider.testBook

                    self.librayService.addBookToShoppingCart(firstBook)
                    self.librayService.addBookToShoppingCart(secondBook)
                    self.librayService.addBookToShoppingCart(testBook)

                    waitUntil(timeout: 5, action: { (done) in
                        self.librayService.booksOffer(with: shoppingCart.booksIds, completion: { (error) in
                            
                            done()
                        })
                    })
                    
                    expect(shoppingCart.finalPrice) == 329
                })
            })
        }
    }
}
